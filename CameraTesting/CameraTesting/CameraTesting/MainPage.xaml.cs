﻿using Plugin.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Plugin.Messaging;
using Syncfusion.Pdf;
using System.IO;
using System.Reflection;
using Syncfusion.Pdf.Parsing;

namespace CameraTesting
{
    public partial class MainPage : ContentPage
    {
        string testPath;
        string imagePath;
        string appPath;
        string dataPath;

        public MainPage()
        {
            InitializeComponent();
            AskPermission();
            TestFunc("test");
        }

        async void TestFunc(string stuff)
        {
            testPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), stuff);
            string written = "hello";
            File.WriteAllText(testPath, written);
            await DisplayAlert("V", testPath, "o");
        }

        async void AskPermission()
        {
            await CrossMedia.Current.Initialize();

            await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);

            await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);
        }

        async void LoadPDF(object sender, EventArgs e)
        {
            dataPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
            appPath = Path.Combine(dataPath.ToString(), "application.pdf");

            Stream docStream = typeof(App).GetTypeInfo().Assembly.GetManifestResourceStream("CameraTesting.client_application_forms.pdf");

            PdfLoadedDocument loadedDocument = new PdfLoadedDocument(docStream);

            PdfLoadedForm form = loadedDocument.Form;

            int lastField = form.Fields.Count - 1;

            (form.Fields[0] as PdfLoadedTextBoxField).Text = "Mike";
            (form.Fields[1] as PdfLoadedTextBoxField).Text = "Also Mike";
            (form.Fields[lastField - 1] as PdfLoadedTextBoxField).Text = "Michael Mahung";
            (form.Fields[lastField] as PdfLoadedTextBoxField).Text = DateTime.Now.ToString();

            (form.Fields[10] as PdfLoadedCheckBoxField).Checked = true;

            FileStream test = File.Create(appPath);

            loadedDocument.Save(test);

            await DisplayAlert("Hmm", File.Exists(appPath).ToString(), "Ok");

            loadedDocument.Close(true);

            await DisplayAlert("Saved", appPath, "Ok");

            string stuffString = File.ReadAllText(testPath);

            

            await DisplayAlert("stuff", stuffString, "ok");

            docStream.Dispose();
            test.Dispose();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("No Camera", ":( No camera available.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(
                new StoreCameraMediaOptions
                {
                    PhotoSize = PhotoSize.Medium,
                    SaveToAlbum = true,
                    Name = "photo"
                });

            if (file == null)
                return;

            await DisplayAlert("Saved Image", "Image Saved Successfully", "OK");

            MainImage.Source = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                imagePath = file.Path;
                file.Dispose();
                return stream;
            });
        }

        private async void Button_Clicked_1(object sender, EventArgs e)
        {
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await DisplayAlert("Error opening gallery", "Image Gallery is not Accessable", "Ok");
                return;
            }

            var file = await CrossMedia.Current.PickPhotoAsync(
                new PickMediaOptions
                {
                    PhotoSize = PhotoSize.Medium
                });

            if (file == null)
                return;

            MainImage.Source = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                imagePath = file.Path;
                file.Dispose();
                return stream;
            });
        }

        private async void Button_Clicked_2(object sender, EventArgs e)
        {
            /*if (string.IsNullOrEmpty(imagePath))
            {
                await DisplayAlert("Error", "Must select an image before sending data", "Ok");
                return;
            }*/

            var emailSender = CrossMessaging.Current.EmailMessenger;

            if (!emailSender.CanSendEmail || !emailSender.CanSendEmailAttachments)
            {
                await DisplayAlert("Error Encounterer", "There was an errer sending the email, make sure email permissions are set and you are connected to the internet", "Ok");
                return;
            }else
            {
                await DisplayAlert("Hmm?", emailSender.CanSendEmail.ToString(), "ok");
            }

            try
            {
                var email = new EmailMessageBuilder()
                .WithAttachment(imagePath, ".jpg")
                .WithAttachment(testPath, ".pdf")
                .To("elmorya.camexp@gmail.com")
                .Subject("Test Email")
                .Body("this email was sent: " + DateTime.Now)
                .Build();

                emailSender.SendEmail(email);

                await DisplayAlert("Sent!", "Message has been sent successfully!", "Ok");
            }
            catch (Exception exception)
            {
                await DisplayAlert("Error", exception.Message, "Ok");
            }

        }
    }
}
